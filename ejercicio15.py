# -*- coding: utf-8 -*-
"""ejercicio15

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1cPUaiEM9ZwqqJ_B7-Xs52DjGQE0suV1F
"""



# Ejercicios Python – 15
# PRE: La entrada son 3 numeros
# POST:el resultado es si el tercer numero ingresado pertenece al intervalo de los dos primeros.
numero1=int(input('numero1 '))
numero2=int(input('numero2 '))
numero3=int(input('numero3 '))
cota_inf=''
if numero1>numero2:
    cota_inf=numero2
    cota_sup=numero1
else:
    cota_inf=numero1
    cota_sup=numero2

if numero3>cota_inf and numero3<cota_sup:
    print(f'{numero3} pertenece al intervalo ({cota_inf},{cota_sup})')
else:
    print(f'{numero3} no pertenece al intervalo ({cota_inf},{cota_sup})')