# -*- coding: utf-8 -*-
"""ejercicio10

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1cPUaiEM9ZwqqJ_B7-Xs52DjGQE0suV1F
"""



# Ejercicios Python – 10
# PRE: Los números son dos valores enteros
# POST:el resultado es el procedimiento de la suma igual a el resultado
numero=int(input('ingrese numero'))
numero_suma=int(input('ingrese numero para sumar'))
if numero_suma<0:
    modulo_numero_suma=numero_suma*-1
else:
    modulo_numero_suma=numero_suma
suma_lenta=''
suma=numero+numero_suma
resultado=''
for all in range(modulo_numero_suma):
    if numero_suma>0:
        suma_lenta+='+1'
        resultado=f'{numero}{suma_lenta}={suma}'
    if numero_suma<0:
        suma_lenta+='-1'
        resultado=f'{numero}{suma_lenta}={suma}'
print(resultado)